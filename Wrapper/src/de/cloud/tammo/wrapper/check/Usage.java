package de.cloud.tammo.wrapper.check;

import com.sun.management.OperatingSystemMXBean;
import de.cloud.tammo.communication.packets.information.WrapperInfoPacket;
import de.cloud.tammo.wrapper.MasterData;
import de.cloud.tammo.wrapper.Wrapper;

import java.lang.management.ManagementFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class Usage {

    private Thread thread;
    private ExecutorService executorService;
    private boolean run;
    private OperatingSystemMXBean os;

    public Usage() {
        this.executorService = Executors.newCachedThreadPool();
        this.run = false;
        os = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        start();
    }

    private void start(){
        executorService.submit(() -> {
           thread = new Thread(new Runnable() {
               @Override
               public void run() {
                   run = true;
                   while(run){
                       //Hier geht es los
                       Wrapper.getServer().sendPacket(new WrapperInfoPacket(getRamAuslastung(), getCPUPercentage()), MasterData.ip, MasterData.port);
                       try {
                           thread.sleep(4000);
                       } catch (InterruptedException e) {
                           e.printStackTrace();
                       }
                   }
               }
           });
           thread.start();
        });
    }

    public void stop(){
        run = false;
    }

    private long getRamAuslastung(){
        return os.getFreePhysicalMemorySize();
    }

    private int getCPUPercentage(){
        double fromos = os.getSystemCpuLoad();
        double cpudouble = fromos * 100.0;
        int cpu = (int) cpudouble;
        return cpu;
    }

}
