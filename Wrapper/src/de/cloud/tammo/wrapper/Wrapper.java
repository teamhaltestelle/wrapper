package de.cloud.tammo.wrapper;

import de.cloud.tammo.communication.PacketServer;
import de.cloud.tammo.communication.handler.ConnectWrapperHandler;
import de.cloud.tammo.communication.handler.DisconnectWrapperHandler;
import de.cloud.tammo.communication.handler.StartServerHandler;
import de.cloud.tammo.communication.packets.connect.WrapperConnectPacket;
import de.cloud.tammo.communication.packets.connect.WrapperDisconnectPacket;
import de.cloud.tammo.wrapper.check.Usage;
import de.cloud.tammo.wrapper.command.Command;
import de.cloud.tammo.wrapper.command.CommandManager;
import de.cloud.tammo.wrapper.config.IPConfig;
import de.cloud.tammo.wrapper.logger.Logger;
import de.cloud.tammo.wrapper.server.GameServer;
import de.cloud.tammo.wrapper.server.ServerHandler;
import de.cloud.tammo.wrapper.template.TemplateHandler;

import java.util.Scanner;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Wrapper {

    private static PacketServer server;
    private static TemplateHandler templateHandler;
    private static ServerHandler serverHandler;
    private static IPConfig ipConfig;
    private static Usage usage;

    public static void main(String[] args){
        new Wrapper();
    }

    public Wrapper (){

        MasterData.connected = false;

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Shutdown();
        }));

        Logger.addMessage("Starte Wrapper...");

        try{
            Thread.sleep(100);

            Logger.addMessage(" _____     ____   _      _____   _   _   _____  "); Thread.sleep(50);
            Logger.addMessage("|__ __|   |  __| | |    | ___ | | | | | |  __ | "); Thread.sleep(50);
            Logger.addMessage("  | |     | |    | |    ||   || | | | | | |  | |"); Thread.sleep(50);
            Logger.addMessage("  | |     | |__  | |__  ||___|| | |_| | | |__| |"); Thread.sleep(50);
            Logger.addMessage("  |_|     |____| |____| |_____| |_____| |_____| "); Thread.sleep(50);

            Logger.addMessage(" "); Thread.sleep(100);
            Logger.addMessage("Copyright by Tammo [Haltestelle]"); Thread.sleep(100);
            Logger.addMessage(" "); Thread.sleep(100);
        }catch (InterruptedException e){}

        ipConfig = new IPConfig();

        setupServer();

        new CommandManager();

        templateHandler = new TemplateHandler();

        serverHandler = new ServerHandler();

        Scanner sc = new Scanner(System.in);
        String line;

        setup();

        usage = new Usage();

        while ((line = sc.nextLine()) != null){
            boolean wasfound = false;
            final String[] arguments = line.split(" ");
            final String cmd = arguments[0];
            for(Command c : CommandManager.getCommands()){
                if (c.getCommand().equalsIgnoreCase(cmd)) {
                    wasfound = true;
                    final String[] arg = new String[arguments.length - 1];
                    System.arraycopy(arguments, 1, arg, 0, arg.length);
                    if(arg.length != 0){
                        if(arg[0].equalsIgnoreCase("help")){
                            c.sendhelp();
                        }
                    }
                    c.execute(arg);
                }
                if(c.getAliases().length != 0){
                    for(String alias : c.getAliases()){
                        if (alias.equalsIgnoreCase(cmd)) {
                            wasfound = true;
                            final String[] arg = new String[arguments.length - 1];
                            System.arraycopy(arguments, 1, arg, 0, arg.length);
                            if(arg.length != 0){
                                if(arg[0].equalsIgnoreCase("help")){
                                    c.sendhelp();
                                }
                            }
                            c.execute(arg);
                        }
                    }
                }
            }
            if(!wasfound){
                Logger.addMessage("Diesen Command gibt es nicht!");
            }
        }
    }

    private void setup(){
        if(!MasterData.connected){
            Logger.addMessage("Warte auf Verbindung vom Master Server...");
            server.sendPacket(new WrapperConnectPacket(true), "127.0.0.1", 225);
            while(!MasterData.connected){
                System.out.flush();
            }
        }
    }

    private void setupServer(){
        server = new PacketServer(226);
        server.bind();
        server.addHandler("CONNECTWRAPPER", new ConnectWrapperHandler());
        server.addHandler("DISCONNECTWRAPPER", new DisconnectWrapperHandler());
        server.addHandler("SERVERSTART", new StartServerHandler());
    }

    public static void Shutdown(){
        Logger.addMessage("Stoppe Wrapper...");
        usage.stop();
        for(GameServer s : getServerHandler().getServer()){
            s.stop();
            Logger.addMessage(s.getName() + " stoppt...");
        }
        if(MasterData.connected){
            server.sendPacket(new WrapperDisconnectPacket(), MasterData.ip, 225);
        }
        server.close();
    }

    public static PacketServer getServer() {
        return server;
    }

    public static TemplateHandler getTemplateHandler() {
        return templateHandler;
    }

    public static ServerHandler getServerHandler() {
        return serverHandler;
    }

    public static IPConfig getIpConfig() {
        return ipConfig;
    }
}
