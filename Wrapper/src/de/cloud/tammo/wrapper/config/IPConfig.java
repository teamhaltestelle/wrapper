package de.cloud.tammo.wrapper.config;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class IPConfig {

    private File config = new File("Wrapper", "ipconfig.txt");
    private ArrayList<String> ips = new ArrayList<String>();

    public IPConfig() {
        if(!config.getParentFile().exists()){
            config.getParentFile().mkdirs();
        }
        if(!config.exists()){
            try {
                config.createNewFile();
                addIp("127.0.0.1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        load();
    }

    public void addIp(String ip){
        ips.add(ip);
        try {
            FileOutputStream fos = new FileOutputStream(config);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for(String s : ips){
                bw.write(s);
                bw.newLine();
            }
            bw.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveips(){
        try {
            FileOutputStream fos = new FileOutputStream(config);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            for(String s : ips){
                bw.write(s);
                bw.newLine();
            }
            bw.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<String> getIps() {
        return ips;
    }

    public void load() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(config));
            String line;
            while((line = br.readLine()) != null){
                ips.add(line);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
