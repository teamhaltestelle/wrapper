package de.cloud.tammo.wrapper.logger;

/**
 * Created by Tammo on 27.06.2017.
 */
public enum MessageType {

    NORMAL,
    WARNING,
    ERROR;

}
