package de.cloud.tammo.wrapper.logger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Logger {

    private static String prefix = "[Cloud] ";
    private static SimpleDateFormat timeStempformat = new SimpleDateFormat("[HH:mm:ss]");

    public static void addMessage(final String msg){
        addMessage(msg, MessageType.NORMAL);
    }

    public static void addMessage(final String msg, final MessageType type){
        final String timeStemp = timeStempformat.format(new Date());

        switch (type){
            case WARNING: System.out.println(timeStemp + " [WARNING] " + msg);
                break;

            case ERROR: System.out.println(timeStemp + " [ERROR] " + msg);
            break;

            default: System.out.println(timeStemp + " " +  prefix + msg);
        }

    }

}
