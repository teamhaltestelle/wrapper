package de.cloud.tammo.wrapper.server;

import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class ServerHandler {

    private ArrayList<GameServer> server;

    public ServerHandler() {
        this.server = new ArrayList<>();
    }

    public void addServer(GameServer gameServer){
        server.add(gameServer);
    }

    public void removeServer(GameServer gameServer){
        if(this.server.contains(gameServer)){
            this.server.remove(gameServer);
        }
    }

    public ArrayList<GameServer> getServer() {
        return server;
    }
}
