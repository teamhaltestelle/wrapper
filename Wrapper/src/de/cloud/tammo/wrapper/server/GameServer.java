package de.cloud.tammo.wrapper.server;

import de.cloud.tammo.wrapper.logger.Logger;
import de.cloud.tammo.wrapper.template.Template;
import de.cloud.tammo.wrapper.utils.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tammo on 27.06.2017.
 */
public class GameServer {

    private String name;
    private int port, minRAM, maxRAM;
    private Template template;
    private Process process;

    public GameServer(String name, int port, Template template, int minRAM, int maxRAM) {
        this.name = name;
        this.port = port;
        this.minRAM = minRAM;
        this.maxRAM = maxRAM;
        this.template = template;
    }

    public void start(){
         new Thread(() -> {
            Logger.addMessage("GameServer " + name + " startet auf Port: " + port + "!");

            File tocopy = template.getPath();
            File ziel = new File("Wrapper//temp//" + name);

            if(ziel.exists()){
                FileUtils.deleteDir(ziel);
            }

            FileUtils.copyDir(tocopy, ziel);

            ProcessBuilder builder = new ProcessBuilder("java", "-Xmx" + maxRAM + "M", "-Xms" + minRAM + "M"  , "-jar", "spigot.jar", "-port", this.port + "");
            builder.directory(ziel);

            try {
                process = builder.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8));
            while (process.isAlive())
                try {
                    String line = reader.readLine();
                    if (line != null)
                        System.out.println(reader.readLine());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }*/

        }).start();
    }

    public void stop(){
        try {
            process.destroy();
            File dir = new File("Wrapper//temp//" + name);
            FileUtils.deleteDir(dir);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

}
