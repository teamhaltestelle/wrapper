package de.cloud.tammo.wrapper.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileUtils {
	
	public static void copyDir(File dir, File to){
		if (!to.exists()) {
            to.mkdirs();
        }
        for (File f : dir.listFiles()) {
            if (f.isDirectory()) {
                copyDir(f, new File(String.valueOf(to.getAbsolutePath()) + "/" + f.getName()));
            } else {
                final File n = new File(String.valueOf(to.getAbsolutePath()) + "/" + f.getName());
                try {
                    Files.copy(f.toPath(), n.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	public static void deleteDir(File dir){
		if (dir.isDirectory()) {
            for (File child : dir.listFiles()) {
                deleteDir(child);
            }
            dir.delete();
        } else {
            dir.delete();
        }
	}

}
