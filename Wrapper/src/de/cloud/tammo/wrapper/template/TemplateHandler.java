package de.cloud.tammo.wrapper.template;

import de.cloud.tammo.wrapper.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class TemplateHandler {

    private File dir;
    private ArrayList<Template> templates;

    public TemplateHandler() {
        this.templates = new ArrayList<Template>();
        this.dir = new File("Wrapper//Templates");
        if(!this.dir.exists()){
            this.dir.mkdirs();
        }
        load();
    }

    public void reload(){
        this.templates.clear();
        load();
    }

    public void load(){
        this.templates.clear();
        for(File temp : dir.listFiles()){
            Template t = new Template(temp.getName());
            this.templates.add(t);
        }
    }

    public File getDir() {
        return dir;
    }

    public ArrayList<Template> getTemplates() {
        return templates;
    }

    public Template getTemplateByName(String name){
        for(Template t : getTemplates()){
            if(t.getName().equalsIgnoreCase(name)){
                return t;
            }
        }
        return null;
    }

    public void deleteTemplate(Template t){
        if(this.templates.contains(t)){
            this.templates.remove(t);
        }
        FileUtils.deleteDir(t.getPath());
    }

}
