package de.cloud.tammo.wrapper.template;

import java.io.*;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Template {

    private String name;
    private File path;

    public Template(String name) {
        this.name = name;
        this.path = new File("Wrapper//Templates//" + name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getPath() {
        return path;
    }

    public void setPath(File path) {
        this.path = path;
    }

}
