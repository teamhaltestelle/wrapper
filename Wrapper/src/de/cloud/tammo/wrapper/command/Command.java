package de.cloud.tammo.wrapper.command;

import de.cloud.tammo.wrapper.logger.Logger;

/**
 * Created by Tammo on 27.06.2017.
 */
public class Command {

    private String command;
    private String[] aliases;

    public Command(String command) {
        this.command = command;
        this.aliases = new String[]{};
    }

    public Command(String command, String... aliases) {
        this.command = command;
        this.aliases = aliases;
    }

    public void execute(String[] args){}

    public void sendhelp(){}

    public String[] getAliases() {
        return aliases;
    }

    public String getCommand() {
        return command;
    }

    public void display(String msg){
        Logger.addMessage(msg);
    }

}
