package de.cloud.tammo.wrapper.command;

import de.cloud.tammo.wrapper.command.executor.StopCommand;
import de.cloud.tammo.wrapper.command.executor.TemplateCommand;

import java.util.ArrayList;

/**
 * Created by Tammo on 27.06.2017.
 */
public class CommandManager {

    private static ArrayList<Command> commands;

    public CommandManager() {
        commands = new ArrayList<>();
        registerCommands();
    }

    private void registerCommands(){
        commands.add(new StopCommand());
        commands.add(new TemplateCommand());
    }

    public static ArrayList<Command> getCommands() {
        return commands;
    }

}
