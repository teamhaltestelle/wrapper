package de.cloud.tammo.wrapper.command.executor;

import de.cloud.tammo.wrapper.Wrapper;
import de.cloud.tammo.wrapper.command.Command;

/**
 * Created by Tammo on 27.06.2017.
 */
public class StopCommand extends Command{

    public StopCommand(){
        super("stop", "s");
    }

    public void execute(String[] args) {
        System.exit(0);
    }
}
