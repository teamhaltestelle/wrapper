package de.cloud.tammo.wrapper.command.executor;

import de.cloud.tammo.wrapper.Wrapper;
import de.cloud.tammo.wrapper.command.Command;
import de.cloud.tammo.wrapper.template.Template;

/**
 * Created by Tammo on 29.06.2017.
 */
public class TemplateCommand extends Command{

    public TemplateCommand() {
        super("template", "temp", "t");
    }

    public void execute(String[] args) {
        if(args.length == 2){
            if(args[0].equalsIgnoreCase("delete")){
                Template template = Wrapper.getTemplateHandler().getTemplateByName(args[1]);
                if(template != null){
                    Wrapper.getTemplateHandler().deleteTemplate(template);
                    display("Du hast das Template " + template.getName() + " erfolgreich gelöscht!");
                }else{
                    display("Dieses Template existiert nicht!");
                }
            }else{
                sendhelp();
            }
        }else if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                display("");
                display("----Template List----");
                for (Template template : Wrapper.getTemplateHandler().getTemplates()) {
                    display(template.getName());
                }
                display("");
            }else if(args[0].equalsIgnoreCase("rl")){
                Wrapper.getTemplateHandler().reload();
                display("Du hast die Templates erneut geladen!");
            }else if(args[0].equalsIgnoreCase("reload")){
                Wrapper.getTemplateHandler().reload();
                display("Du hast die Templates erneut geladen!");
            }else{
                sendhelp();
            }
        }else{
            sendhelp();
        }
    }

    public void sendhelp() {
        display("Template delete <Name>");
        display("Template list");
        display("Template reload");
    }
}
