package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.ErrorPacket;
import de.cloud.tammo.communication.packets.SuccessPacket;

import java.net.Socket;

/**
 * Created by Tammo on 26.06.2017.
 */
public class ErrorPacketHandler extends Handler{

    @Override
    public Packet handle(Packet p, Socket socket) {
        ErrorPacket packet = (ErrorPacket) p;
        System.out.println("[ERROR] " + packet.getError());
        return new SuccessPacket();
    }

}
