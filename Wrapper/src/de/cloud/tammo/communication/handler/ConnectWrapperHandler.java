package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.connect.ConnectWrapperPacket;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.communication.packets.connect.WrapperConnectPacket;
import de.cloud.tammo.wrapper.MasterData;
import de.cloud.tammo.wrapper.Wrapper;
import de.cloud.tammo.wrapper.logger.Logger;

import java.net.Socket;

/**
 * Created by Tammo on 27.06.2017.
 */
public class ConnectWrapperHandler extends Handler{

    @Override
    public Packet handle(Packet p, Socket socket) {
        String ip = socket.getInetAddress().getHostAddress();
        ConnectWrapperPacket packet = (ConnectWrapperPacket) p;
        if(!packet.isLater()){
            Wrapper.getServer().sendPacket(new WrapperConnectPacket(false), socket.getInetAddress().getHostAddress(), 225);
        }
        MasterData.connected = true;
        MasterData.ip = ip;
        Logger.addMessage("Master Server ist nun verbunden!");
        return new SuccessPacket();
    }
}
