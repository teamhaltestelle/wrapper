package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.wrapper.MasterData;
import de.cloud.tammo.wrapper.logger.Logger;

import java.net.Socket;

/**
 * Created by Tammo on 27.06.2017.
 */
public class DisconnectWrapperHandler extends Handler{

    @Override
    public Packet handle(Packet p, Socket socket) {
        MasterData.connected = false;
        Logger.addMessage("Der Master GameServer hat sich verabschiedet...");
        System.exit(0);
        return new SuccessPacket();
    }

}
