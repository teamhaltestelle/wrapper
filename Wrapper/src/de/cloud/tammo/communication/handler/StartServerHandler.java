package de.cloud.tammo.communication.handler;

import de.cloud.tammo.communication.Handler;
import de.cloud.tammo.communication.Packet;
import de.cloud.tammo.communication.packets.ErrorPacket;
import de.cloud.tammo.communication.packets.information.StartServerPacket;
import de.cloud.tammo.communication.packets.SuccessPacket;
import de.cloud.tammo.wrapper.Wrapper;
import de.cloud.tammo.wrapper.logger.Logger;
import de.cloud.tammo.wrapper.server.GameServer;
import de.cloud.tammo.wrapper.template.Template;

import java.net.Socket;

/**
 * Created by Tammo on 28.06.2017.
 */
public class StartServerHandler extends Handler{


    public Packet handle(Packet p, Socket socket) {
        StartServerPacket packet = (StartServerPacket) p;
        Template template = Wrapper.getTemplateHandler().getTemplateByName(packet.getTemplate());
        if(template != null){
            GameServer server = new GameServer(packet.getServerName(), packet.getPort(), template, packet.getMinRAM(), packet.getMaxRAM());
            Wrapper.getServerHandler().addServer(server);
            server.start();
        }else{
            Logger.addMessage("Dieses Template existiert nicht!");
            Wrapper.getServer().sendPacket(new ErrorPacket("Dieses Template existiert nicht!"), socket.getInetAddress().getHostAddress(), 225);
        }
        return new SuccessPacket();
    }
}
