package de.cloud.tammo.communication;

import java.net.Socket;

/**
 * Created by Tammo on 26.06.2017.
 */
public abstract class Handler {

    public Handler() {}

    public abstract Packet handle(Packet p, Socket socket);

}
