package de.cloud.tammo.communication.packets.information;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 30.06.2017.
 * Copyright by Tammo
 */
public class StopServerPacket extends Packet{

    private String servername;

    public StopServerPacket(String servername) {
        super("SERVERSTOP");
        this.servername = servername;
    }

    public String getServername() {
        return servername;
    }
}
