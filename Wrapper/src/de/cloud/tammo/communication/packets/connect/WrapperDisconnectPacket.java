package de.cloud.tammo.communication.packets.connect;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 27.06.2017.
 */
public class WrapperDisconnectPacket extends Packet{

    public WrapperDisconnectPacket() {
        super("WRAPPERDISCONNECT");
    }
}
