package de.cloud.tammo.communication.packets;

import de.cloud.tammo.communication.Packet;

/**
 * Created by Tammo on 26.06.2017.
 */
public class ErrorPacket extends Packet{

    private String Error;

    public ErrorPacket(String Error) {
        super("ERROR");
        this.Error = Error;
    }

    public String getError() {
        return Error;
    }

}
