package de.cloud.tammo.communication;

import java.io.Serializable;

/**
 * Created by Tammo on 26.06.2017.
 */
public class Packet implements Serializable{

    private String name;

    public Packet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
